datasource db {
  provider = "mongodb"
  url      = env("DATABASE_URL")
}

generator client {
  provider = "prisma-client-js"
}

generator dbml {
  provider = "prisma-dbml-generator"
}

generator nestgraphql {
  provider = "node node_modules/prisma-nestjs-graphql"
  output   = "../src/@generated"

  fields_Place_from   = "../../custom_generator/place"
  fields_Place_input  = true
  fields_Place_output = true

  fields_Photo_from   = "../../custom_generator/photo"
  fields_Photo_input  = true
  fields_Photo_output = true

  fields_PaymentToken_from   = "../../custom_generator/payment_token"
  fields_PaymentToken_input  = true
  fields_PaymentToken_output = true
}

model User {
  id            String  @id @default(auto()) @map("_id") @db.ObjectId
  email         String? @unique
  emailVerified Boolean @default(false)
  phoneNumber   String? @unique
  name          String?
  /// @HideField()
  hash          String?
  tokens        Token[]

  /// @FieldType('Photo.PhotoUrl')
  photoUrl Json?

  author Author?
  order  Order[]

  /// @FieldType('PaymentToken.PaymentToken')
  paymentToken Json[]
  Finance      Finance[]
  partyComing  PartyComing[]
}

model Author {
  id       String @id @default(auto()) @map("_id") @db.ObjectId
  /// @FieldType('Photo.PhotoUrl')
  photoUrl Json?
  user     User   @relation(fields: [userId], references: [id])
  userId   String @unique @db.ObjectId

  parties Party[] @relation(name: "parties_author")

  musics Music[] @relation(name: "music_author")
  order  Order[]

  mucicFolder MusicFolder[] @relation(name: "mucicFolder_author")
  isApproved  Boolean
  data        Json?
  Finance     Finance[]
}

model Token {
  id           String  @id @default(auto()) @map("_id") @db.ObjectId
  accessToken  String?
  refreshToken String?
  device       String?
  fcmToken     String?
  userId       String  @db.ObjectId
  user         User    @relation(fields: [userId], references: [id])

  @@unique([userId, device], name: "userIdDevice")
}

enum PartyStatus {
  ongoing
  started
  finished
  canceled
}

model Party {
  id            String      @id @default(auto()) @map("_id") @db.ObjectId
  dateStart     DateTime
  dateEnd       DateTime
  name          String
  description   String?
  status        PartyStatus @default(ongoing)
  tags          String[]
  /// @FieldType('Photo.PhotoUrl')
  photoUrl      Json?
  placeType     String?
  additionalTag String[]
  typeTag       String[]

  /// @FieldType('Place.Place')
  place    Json
  city     String
  authorId String  @db.ObjectId
  author   Author  @relation(name: "parties_author", fields: [authorId], references: [id])
  orders   Order[]

  musicFolderIds String[]      @db.ObjectId
  musicFolder    MusicFolder[] @relation(fields: [musicFolderIds], references: [id])

  partyComing PartyComing[]
}

model PartyComing {
  id String @id @default(auto()) @map("_id") @db.ObjectId
  // 

  partyId String @db.ObjectId
  party   Party  @relation(fields: [partyId], references: [id])
  // 

  userId String @db.ObjectId
  user   User   @relation(fields: [userId], references: [id])
}

model MusicFolder {
  id String @id @default(auto()) @map("_id") @db.ObjectId

  name     String
  partyIds String[] @db.ObjectId
  party    Party[]  @relation(fields: [partyIds], references: [id])
  musicIds String[] @db.ObjectId
  music    Music[]  @relation(fields: [musicIds], references: [id])
  author   Author   @relation(name: "mucicFolder_author", fields: [authorId], references: [id])
  authorId String   @db.ObjectId
}

model Music {
  id             String        @id @default(auto()) @map("_id") @db.ObjectId
  url            String
  name           String
  label          String
  price          Float?
  Order          Order[]
  authorId       String?       @db.ObjectId
  author         Author?       @relation(fields: [authorId], references: [id], name: "music_author")
  musicFolderIds String[]      @db.ObjectId
  musicFolder    MusicFolder[] @relation(fields: [musicFolderIds], references: [id])
}

enum OrderStatus {
  offer
  accepted
  declined
  started
  finished
}

model Order {
  id         String      @id @default(auto()) @map("_id") @db.ObjectId
  comment    String?
  price      Float
  priority   Int         @default(0)
  acceptTime DateTime?
  startTime  DateTime?
  finishTime DateTime?
  status     OrderStatus
  authorId   String      @db.ObjectId
  author     Author      @relation(fields: [authorId], references: [id])
  musicId    String      @db.ObjectId
  music      Music       @relation(fields: [musicId], references: [id])
  userId     String      @db.ObjectId
  user       User        @relation(fields: [userId], references: [id])
  partyId    String      @db.ObjectId
  party      Party       @relation(fields: [partyId], references: [id])
  model      Json?

  finances Finance[]
}

model Finance {
  id      String  @id @default(auto()) @map("_id") @db.ObjectId
  orderId String? @db.ObjectId
  order   Order?  @relation(fields: [orderId], references: [id])

  userId String @db.ObjectId
  user   User   @relation(fields: [userId], references: [id])

  authorId String @db.ObjectId
  author   Author @relation(fields: [authorId], references: [id])

  amount Float
  date   DateTime

  transaction String
}
