import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { UserModule } from './modules/user/user.module';
import { PrismaModule } from './modules/prisma/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { MongoDBModule } from './modules/mongo/mongo.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PartyModule } from './modules/party/party.module';
import { privateKey, publicKey } from './utils/jwt_keys';
import { OrderModule } from './modules/order/order.module';
import * as jwt from 'jsonwebtoken';
import { MusicModule } from './modules/music/music.module';
import { TelegramModule } from './modules/telegram/telegram.module';
import { ScheduleModule } from '@nestjs/schedule';
import { WhatsappModule } from './modules/whatsapp/whatsapp.module';
import { MeiliSearchModule } from 'nestjs-meilisearch';
import { FolderModule } from './modules/folder/folder.module';
import { globalPrismaClient } from './modules/prisma/prisma.service';
import { PaymentModule } from './modules/payment/payment.module';
import { ArtistModule } from './modules/artist/artist.module';
import { FinanceModule } from './modules/finance/finance.module';
import { GRPCModule } from './modules/grpc/grpc.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MeiliSearchModule.forRoot({
      host: 'https://testdjsearch.gacademy.studio',
      apiKey: '123',
    }),
    PrismaModule,
    JwtModule.register({
      global: true,
      privateKey,
      publicKey,
      secret: '123456789',
    }),
    ArtistModule,
    MongoDBModule,
    UserModule,
    PartyModule,
    MusicModule,
    WhatsappModule,
    { ...ScheduleModule.forRoot(), global: true },
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      playground: true,
      autoSchemaFile: true,
      sortSchema: true,
      installSubscriptionHandlers: true,
      csrfPrevention: false,
      subscriptions: {
        'subscriptions-transport-ws': {
          onConnect: async (connectionParams) => {
            const token = connectionParams.Authorization;
            if (token == null) return {};
            const auth = jwt.verify(token, '123456789');
            const author = await globalPrismaClient.author.findFirst({
              where: {
                userId: auth['id'],
              },
            });
            return {
              userId: auth['id'],
              authorId: author?.id,
            };
          },
        },
      },
    }),
    OrderModule,
    TelegramModule,
    FinanceModule,
    FolderModule,
    PaymentModule,
    GRPCModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
