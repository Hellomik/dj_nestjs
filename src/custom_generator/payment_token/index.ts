import { Field, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
@InputType('PaymentTokenInput')
export class PaymentToken {
  @Field()
  id: string;

  @Field()
  cardType: string;

  @Field()
  cardLastFour: string;
}
