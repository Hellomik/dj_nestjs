import { Field, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
@InputType('PhotoUrlInput')
export class PhotoUrl {
  @Field()
  little: string;

  @Field()
  large: string;

  @Field()
  xLarge: string;
}
