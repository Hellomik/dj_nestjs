import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { LatLng } from './lat_lng';

@ObjectType()
@InputType('PlaceInput')
export class Place {
  @Field(() => LatLng)
  latLng: LatLng;

  @Field()
  address: string;

  @Field({ nullable: true })
  label?: string;
}
