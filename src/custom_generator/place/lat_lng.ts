import { Field, Float, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
@InputType('LatLngInput')
export class LatLng {
  @Field(() => Float)
  lat: number;

  @Field(() => Float)
  long: number;
}
