import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import graphqlUploadExpress from './utils/graphql_upload/graphqlUploadExpress';
import { grpcClientOptions } from './utils/grpc_option';
import { MicroserviceOptions } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use('/graphql', graphqlUploadExpress({}));

  await app.listen(3009);

  const appGrpc = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    grpcClientOptions,
  );

  appGrpc.listen();
}
bootstrap();
