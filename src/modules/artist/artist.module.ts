import { Module } from '@nestjs/common';
import { RequestController } from './controller/request.controller';
import { ArtistResolver } from './resolver/artist.resolver';

@Module({
  controllers: [RequestController],
  providers: [ArtistResolver],
})
export class ArtistModule {}
