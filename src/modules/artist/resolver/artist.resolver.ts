import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { bool } from 'aws-sdk/clients/signer';
import GraphQLUpload from 'src/utils/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/utils/graphql_upload/Upload';
import { transporter } from '../controller/request.controller';
import { UseGuards } from '@nestjs/common';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { Author } from 'src/@generated/author/author.model';

@Resolver()
export class ArtistResolver {
  //
  constructor(private prismaService: PrismaService) {}

  //
  @UseGuards(PreAuthGuard)
  @Mutation(() => Author)
  async applyArtist(
    @Args('company') company: bool,
    @Args('name') name: string,
    @Args('number') number: string,
    @Args('fullName') fullName: string,
    @Args('numberDocument') numberDocument: string,
    @Args('directorNumber') directorNumber: string,
    @Args('billNumber') billNumber: string,
    @Args('kbe') kbe: string,
    @Args('bik') bik: string,
    @Args('bankName') bankName: string,
    @Args('seldDocs', {
      type: () => [GraphQLUpload],
    })
    seldDocs: Promise<FileUpload>[],
    @Args('signedDocs', {
      type: () => [GraphQLUpload],
    })
    signedDocs: Promise<FileUpload>[],
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<Author> {
    const seldDocsToSend = await Promise.all(seldDocs);
    const signedDocsSend = await Promise.all(signedDocs);
    await transporter.sendMail({
      from: 'info@mynight.kz', // sender address
      to: 'info@mynight.kz', // list of receivers
      subject: `Заявка ${new Date().toISOString()}`, // Subject line
      text: `
      ${company} company
      ${name} name
      ${number} number
      ${fullName} fullName
      ${numberDocument} numberDocument
      ${directorNumber} directorNumber
      ${billNumber} billNumber
      ${kbe} kbe
      ${bik} bik
      ${bankName} bankName
      `,
      attachments: [
        ...seldDocsToSend.map((val) => ({
          filename: val.filename,
          content: val.createReadStream(),
        })),
        ...signedDocsSend.map((val) => ({
          filename: val.filename,
          content: val.createReadStream(),
        })),
      ],
    });
    const data = await this.prismaService.author.create({
      data: {
        user: {
          connect: {
            id: userId,
          },
        },
        isApproved: false,
        data: {
          company,
          name,
          number,
          fullName,
          numberDocument,
          directorNumber,
          billNumber,
          kbe,
          bik,
          bankName,
        },
      },
    });
    return data;
  }

  @UseGuards(PreAuthGuard)
  @Query(() => Author, {
    nullable: true,
  })
  async checkAuthorIsExist(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<Author | null> {
    const data = await this.prismaService.author.findFirst({
      where: {
        userId,
      },
    });
    if (data == null) {
      return null;
    }
    return data;
  }
}
