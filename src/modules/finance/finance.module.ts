import { Module } from '@nestjs/common';
import { ApplyFinances } from './services/apply_finance.service';
import { AuthorFinanceResolver } from './resolvers/author.resolver';

@Module({
  providers: [ApplyFinances, AuthorFinanceResolver],
  exports: [ApplyFinances],
})
export class FinanceModule {}
