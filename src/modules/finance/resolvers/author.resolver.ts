import { UseGuards } from '@nestjs/common';
import { Args, GraphQLISODateTime, Query, Resolver } from '@nestjs/graphql';
import { Finance } from 'src/@generated/finance/finance.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';

@Resolver()
export class AuthorFinanceResolver {
  constructor(private prismaService: PrismaService) {}

  @UseGuards(PreAuthGuard)
  @Query(() => [Finance])
  async getFinances(
    @Args('firstDate', {
      type: () => GraphQLISODateTime,
      nullable: true,
    })
    firstDate: Date,
    @Args('endDate', {
      type: () => GraphQLISODateTime,
      nullable: true,
    })
    endDate: Date,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<Finance[]> {
    const searchBody = {};
    if (firstDate != null) {
      searchBody['gte'] = firstDate;
    }
    if (endDate != null) {
      searchBody['lte'] = endDate;
    }
    return this.prismaService.finance.findMany({
      where: {
        date: searchBody,
        author: {
          userId,
        },
      },
      include: {
        order: {
          include: {
            party: true,
          },
        },
      },
    });
  }
}
