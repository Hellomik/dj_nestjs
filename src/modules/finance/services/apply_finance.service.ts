import { Injectable } from '@nestjs/common';
import { Finance, Order } from '@prisma/client';
import { PrismaService } from 'src/modules/prisma/prisma.service';

@Injectable()
export class ApplyFinances {
  constructor(private prismaService: PrismaService) {}

  async registerTransaction(
    order: Order,
    transaction: string,
  ): Promise<Finance> {
    return this.prismaService.finance.create({
      data: {
        orderId: order.id,
        userId: order.userId,
        amount: order.price,
        date: new Date(),
        authorId: order.authorId,
        transaction: transaction.toString(),
      },
    });
  }
}
