import { Module } from '@nestjs/common';
import { CreateFolderResolver } from './resolvers/create_folder.resolver';

@Module({
  providers: [CreateFolderResolver],
})
export class FolderModule {}
