import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { MusicFolder } from 'src/@generated/music-folder/music-folder.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';

@Resolver()
export class CreateFolderResolver {
  constructor(private prismaService: PrismaService) {}

  @UseGuards(PreAuthGuard)
  @Mutation(() => MusicFolder)
  async createFolder(
    @Args('name') name: string,
    @Args('musicIds', { type: () => [String] }) musicIds: string[],
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<MusicFolder> {
    const musicSet = new Set<string>(musicIds);
    const newMusicFolder = await this.prismaService.musicFolder.create({
      data: {
        name,
        author: {
          connect: {
            userId,
          },
        },
        music: {
          connect: [...musicSet.values()].map((e) => ({
            id: e,
          })),
        },
      },
    });
    return newMusicFolder;
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => MusicFolder)
  async editFolder(
    @Args('id') id: string,
    @Args('name') name: string,
    @Args('musicIds', { type: () => [String] }) musicIds: string[],
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<MusicFolder> {
    const musicSet = new Set<string>(musicIds);
    const newMusicFolder = await this.prismaService.musicFolder.update({
      where: {
        id,
        author: {
          userId,
        },
      },
      data: {
        name,
        music: {
          set: [...musicSet.values()].map((e) => ({
            id: e,
          })),
          // connect: [...musicSet.values()].map((e) => ({
          //   id: e,
          // })),
        },
      },
    });
    return newMusicFolder;
  }

  @Query(() => MusicFolder)
  async getMusicsByFolderId(
    @Args('folderId') folderId: string,
  ): Promise<MusicFolder> {
    return await this.prismaService.musicFolder.findFirst({
      where: {
        id: folderId,
      },
      include: {
        music: true,
      },
    });
  }

  @Query(() => [MusicFolder])
  @UseGuards(PreAuthGuard)
  async getAllFoldersWithMusicByAuthorId(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<MusicFolder[]> {
    return this.prismaService.musicFolder.findMany({
      where: {
        author: {
          userId,
        },
      },
      include: {
        music: true,
      },
    });
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => Boolean)
  async deleteFolder(@Args('id') id: string) {
    await this.prismaService.musicFolder.delete({
      where: {
        id,
      },
    });
    return true;
  }
}
