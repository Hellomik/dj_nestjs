import { Module } from '@nestjs/common';
import { GrpcReflectionModule } from 'nestjs-grpc-reflection';
import { grpcClientOptions } from 'src/utils/grpc_option';
import { PartyClientController } from './modules/order/order.controller';

@Module({
  imports: [GrpcReflectionModule.register(grpcClientOptions)],
  controllers: [PartyClientController],
})
export class GRPCModule {}
