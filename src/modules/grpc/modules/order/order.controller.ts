import { Metadata } from '@grpc/grpc-js';
import {
  ServerDuplexStreamImpl,
  ServerWritableStreamImpl,
} from '@grpc/grpc-js/build/src/server-call';
import { Controller, UseFilters } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ObjectId } from 'mongodb';
import { Observable, Subject } from 'rxjs';
import {
  EmptyBody,
  ListenPartyInput,
  ListenPartyOutput,
  PartyServiceClient,
  PartyServiceControllerMethods,
} from 'src/proto/dj_night';
import { getUserIdByMetadata } from 'src/utils/getUserIdByMetadata';
import { Http2gRPCExceptionFilter } from 'src/utils/http2gRPCException.filter';

@Controller()
@PartyServiceControllerMethods()
export class PartyClientController implements PartyServiceClient {
  constructor(private jwtService: JwtService) {}

  partyListenMap: Map<string, Map<string, Subject<ListenPartyOutput>>> =
    new Map<string, Map<string, Subject<any>>>();

  @UseFilters(new Http2gRPCExceptionFilter())
  listenPartyAsClient(
    messages: ListenPartyInput,
    metaData?: Metadata,
    duplex?: ServerWritableStreamImpl<ListenPartyInput, ListenPartyOutput>,
  ): Observable<ListenPartyOutput> {
    if (messages.id == null) throw 'No id';

    const partyId = messages.id;
    const subject = new Subject<ListenPartyOutput>();
    const subjectId = new ObjectId().toHexString();

    const partyMap: Map<
      string,
      Subject<ListenPartyOutput>
    > = this.partyListenMap.get(partyId) ?? new Map();
    this.partyListenMap.set(partyId, partyMap);
    partyMap.set(subjectId, subject);

    const onComplete = () => {
      const partyMap: Map<
        string,
        Subject<ListenPartyOutput>
      > = this.partyListenMap.get(partyId) ?? new Map();
      this.partyListenMap.set(partyId, partyMap);
      const subjectFounded = partyMap.get(subjectId);
      (subjectFounded ?? subject).complete();
      partyMap.delete(subjectId);
    };

    duplex.on('close', () => {
      onComplete();
    });

    return subject.asObservable();
  }

  listenPartyAsDj(request: EmptyBody): Observable<ListenPartyOutput> {
    throw new Error('Method not implemented.');
  }
}
