import { FactoryProvider, Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { Db, MongoClient } from 'mongodb';

export const mongodDbFactory: FactoryProvider<Promise<Db>> = {
  provide: 'DATABASE_CONNECTION',
  useFactory: async (configService: ConfigService): Promise<Db> => {
    try {
      const client = await MongoClient.connect(configService.get('DATABASE_URL'));
      const database = client.db(configService.get('dj'));
      return database;
    } catch (e) {
      throw e;
    }
  },
  inject: [ConfigService],
};

@Global()
@Module({
  imports: [ConfigModule],
  providers: [mongodDbFactory],
  exports: [mongodDbFactory],
})
export class MongoDBModule {}
