import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class EditMusic {
  @Field()
  url: string;

  @Field()
  name: string;

  @Field()
  label: string;

  @Field()
  id: string;

  @Field()
  price: number;
}
