import { Module } from '@nestjs/common';
import { CreateMusicResolver } from './resolvers/create.resolver';
import { GetMusicResolver } from './resolvers/get_music.resolver';

@Module({
  providers: [CreateMusicResolver, GetMusicResolver],
})
export class MusicModule {}
