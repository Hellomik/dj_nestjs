import { UseGuards } from '@nestjs/common';
import { Query, Args, Mutation, Resolver } from '@nestjs/graphql';
import { MusicCreateWithoutAuthorInput } from 'src/@generated/music/music-create-without-author.input';
import { MusicUpdateOneRequiredWithoutOrderNestedInput } from 'src/@generated/music/music-update-one-required-without-order-nested.input';
import { MusicUpdateWithoutOrderInput } from 'src/@generated/music/music-update-without-order.input';
import { MusicUpdateInput } from 'src/@generated/music/music-update.input';
import { Music } from 'src/@generated/music/music.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';
import { EditMusic } from '../model/edit.music';
import { MusicFolder } from 'src/@generated/music-folder/music-folder.model';

@Resolver()
export class CreateMusicResolver {
  constructor(private prismaService: PrismaService) {}

  @UseGuards(PreAuthGuard)
  @Mutation(() => Music)
  async createMusic(
    @Args('music') musicInput: MusicCreateWithoutAuthorInput,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ) {
    const musciCreated = await this.prismaService.music.create({
      data: {
        ...musicInput,
        author: {
          connect: {
            userId,
          },
        },
      },
    });
    return musciCreated;
  }

  @UseGuards(PreAuthGuard)
  @Query(() => [Music])
  async getMusicByAuthor(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<Music[]> {
    const musics = await this.prismaService.music.findMany({
      where: {
        author: {
          userId,
        },
      },
    });
    return musics;
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => Music)
  async editMusic(
    @Args('data', {
      type: () => EditMusic,
    })
    data: EditMusic,
  ) {
    const { id, ...updateData } = data;
    return await this.prismaService.music.update({
      where: {
        id: id,
      },
      data: {
        ...updateData,
      },
    });
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => Boolean)
  async deleteMusic(@Args('id') id: string) {
    const res = await this.prismaService.music.update({
      where: {
        id,
      },
      data: {
        author: {
          disconnect: true,
        },
        musicFolder: {
          set: [],
        },
      },
    });

    return true;
  }

  @Mutation(() => MusicFolder)
  async addMusicToFolder(
    @Args('musicId') musicId: string,
    @Args('folderId') folderId: string,
  ): Promise<MusicFolder> {
    return this.prismaService.musicFolder.update({
      where: {
        id: folderId,
      },
      data: {
        music: {
          connect: {
            id: musicId,
          },
        },
      },
    });
  }
}
