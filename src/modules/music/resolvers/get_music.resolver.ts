import { Args, Query, Resolver } from '@nestjs/graphql';
import { Music } from 'src/@generated/music/music.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';

@Resolver()
export class GetMusicResolver {
  constructor(private prismaService: PrismaService) {}

  @Query(() => [Music])
  async getMusicByPartyId(@Args('partyId') partyId: string): Promise<Music[]> {
    const data = await this.prismaService.party.findFirst({
      where: {
        id: partyId,
      },
      include: {
        author: {
          include: {
            musics: true,
          },
        },
      },
    });
    return data.author.musics;
  }
}
