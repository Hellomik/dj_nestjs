import { Module } from '@nestjs/common';
import { ClientOrderResolver } from './resolvers/client_order.resolver';
import { AuthorOrderResolver } from './resolvers/author_order.resolver';
import { PaymentModule } from '../payment/payment.module';
import { FinanceModule } from '../finance/finance.module';

@Module({
  imports: [PaymentModule, FinanceModule],
  providers: [ClientOrderResolver, AuthorOrderResolver],
})
export class OrderModule {}
