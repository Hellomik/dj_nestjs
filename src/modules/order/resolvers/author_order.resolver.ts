import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Order } from 'src/@generated/order/order.model';
import { ApplyFinances } from 'src/modules/finance/services/apply_finance.service';
import { pubSub } from 'src/modules/party/resolvers/event_sub.resolver';
import { PayService } from 'src/modules/payment/services/pay_service';
import { PrismaService } from 'src/modules/prisma/prisma.service';

@Resolver()
export class AuthorOrderResolver {
  constructor(
    private prismaService: PrismaService,
    private payService: PayService,
    private applyFinances: ApplyFinances,
  ) {}

  @Query(() => [Order])
  async getOrdersByPartyIdAsAuthor(
    @Args('partyId') partyId: string,
  ): Promise<Order[]> {
    return this.prismaService.order.findMany({
      where: {
        partyId,
      },
      include: {
        user: true,
        music: true,
      },
      orderBy: {
        id: 'desc',
      },
    });
  }

  @Mutation(() => Order)
  async declineOrderById(@Args('orderId') orderId: string): Promise<Order> {
    const dataFirst = await this.prismaService.order.findFirst({
      where: {
        id: orderId,
        status: 'offer',
      },
    });

    if ((dataFirst.model as any)?.TransactionId != null) {
      await this.payService.cancelAuth({
        transactionId: dataFirst.model['TransactionId'],
      });
    }

    const data = await this.prismaService.order.update({
      where: {
        id: orderId,
        status: 'offer',
      },
      data: {
        status: 'declined',
      },
      include: {
        music: true,
      },
    });

    pubSub.publish('orderChanged', data);
    return data;
  }

  @Mutation(() => Order)
  async acceptOrderById(@Args('orderId') orderId: string): Promise<Order> {
    const dataFirst = await this.prismaService.order.findFirst({
      where: {
        id: orderId,
        status: 'offer',
      },
    });

    if ((dataFirst.model as any)?.TransactionId != null) {
      await this.payService.confirmAuth({
        amount: dataFirst.price,
        transactionId: dataFirst.model['TransactionId'],
      });
    }

    const data = await this.prismaService.order.update({
      where: {
        id: orderId,
        status: 'offer',
      },
      data: {
        status: 'accepted',
        acceptTime: new Date(),
      },
      include: {
        music: true,
      },
    });
    if ((dataFirst.model as any)?.TransactionId != null) {
      this.applyFinances
        .registerTransaction(data, dataFirst.model['TransactionId'])
        .catch((err) => console.log(err));
    }
    pubSub.publish('orderChanged', data);
    return data;
  }

  @Mutation(() => Order)
  async startOrderById(@Args('orderId') orderId: string): Promise<Order> {
    const data = await this.prismaService.order.update({
      where: {
        id: orderId,
        status: 'accepted',
      },
      data: {
        status: 'started',
        startTime: new Date(),
      },
      include: {
        music: true,
      },
    });
    pubSub.publish('orderChanged', data);
    return data;
  }

  @Mutation(() => Order)
  async finishOrderById(@Args('orderId') orderId: string): Promise<Order> {
    const data = await this.prismaService.order.update({
      where: {
        id: orderId,
        status: 'started',
      },
      data: {
        status: 'finished',
        finishTime: new Date(),
      },
      include: {
        music: true,
      },
    });
    pubSub.publish('orderChanged', data);
    return data;
  }
}
