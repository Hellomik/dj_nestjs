import { Inject, UseGuards } from '@nestjs/common';
import { Args, Float, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { OrderStatus, Party, PartyStatus, Prisma } from '@prisma/client';
import { ApolloError } from 'apollo-server-express';
import { Collection, Db, ObjectId } from 'mongodb';
import { MusicCreateWithoutOrderInput } from 'src/@generated/music/music-create-without-order.input';

import { Order } from 'src/@generated/order/order.model';
import { pubSub } from 'src/modules/party/resolvers/event_sub.resolver';
import { PayService } from 'src/modules/payment/services/pay_service';

import { PrismaService } from 'src/modules/prisma/prisma.service';
import { PaymentTokenModel } from 'src/modules/user/model/payment_token.model';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';

@Resolver()
export class ClientOrderResolver {
  userCollection: Collection<{
    _id: ObjectId;
    paymentToken: PaymentTokenModel[];
  }>;

  constructor(
    @Inject('DATABASE_CONNECTION') private database: Db,
    private prismaService: PrismaService,
    private payService: PayService,
  ) {
    this.userCollection = this.database.collection('User');
  }

  @Mutation(() => Order)
  @UseGuards(PreAuthGuard)
  async createOrder(
    @Args('price', { type: () => Float }) price: number, //: number;
    @Args('musicId') musicId: string, //: MusicCreateNestedOneWithoutOrderInput;
    @Args('partyId') partyId: string,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
    @Args('paymentId') paymentId: string,
    @Args('comment', {
      nullable: true,
    })
    comment?: string,
  ): Promise<Order> {
    const cur = this.userCollection.aggregate<{
      _id: ObjectId;
      paymentToken: PaymentTokenModel[];
    }>([
      {
        $match: {
          'paymentToken._id': new ObjectId(paymentId),
        },
      },
    ]);
    const userPaymentWay = await cur.next();
    cur.close();
    if (userPaymentWay == null) throw 'No Payment Way';
    if (userPaymentWay._id.toHexString() != userId) throw 'Bad hacker';
    const paymentEntity = userPaymentWay.paymentToken.find((val) => {
      return val._id.toHexString() == paymentId;
    });
    if (paymentEntity == null) throw 'No Payment Way';

    const model = await this.payService.payAuth({
      amount: price,
      accountId: userId,
      token: paymentEntity.token,
    });

    const party: Party = await this.prismaService.party.findUnique({
      where: {
        id: partyId,
      },
    });
    if (party == null) throw new ApolloError('Party not found');
    if (party.status != PartyStatus.started)
      throw new ApolloError('Party status not corret');

    const newOrder = await this.prismaService.order.create({
      data: {
        status: OrderStatus.offer,
        price,
        comment,
        model: model as any,
        author: {
          connect: {
            id: party.authorId,
          },
        },
        music: {
          connect: {
            id: musicId,
          },
        },
        user: {
          connect: {
            id: userId,
          },
        },
        party: {
          connect: {
            id: partyId,
          },
        },
      },
      include: {
        music: true,
      },
    });
    pubSub.publish('orderChanged', newOrder);
    return newOrder;
  }

  @Mutation(() => Order)
  @UseGuards(PreAuthGuard)
  async createOrderWithOwnMusic(
    @Args('price', { type: () => Float }) price: number, //: number;
    @Args('music') music: MusicCreateWithoutOrderInput, //: MusicCreateNestedOneWithoutOrderInput;
    @Args('partyId') partyId: string,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
    @Args('paymentId') paymentId: string,
    @Args('comment', {
      nullable: true,
    })
    comment?: string,
  ): Promise<Order> {
    const cur = this.userCollection.aggregate<{
      _id: ObjectId;
      paymentToken: PaymentTokenModel[];
    }>([
      {
        $match: {
          'paymentToken._id': new ObjectId(paymentId),
        },
      },
    ]);
    const userPaymentWay = await cur.next();
    cur.close();
    if (userPaymentWay == null) throw 'No Payment Way';
    if (userPaymentWay._id.toHexString() != userId) throw 'Bad hacker';
    const paymentEntity = userPaymentWay.paymentToken.find((val) => {
      return val._id.toHexString() == paymentId;
    });
    if (paymentEntity == null) throw 'No Payment Way';

    const model = await this.payService.payAuth({
      amount: price,
      accountId: userId,
      token: paymentEntity.token,
    });

    const party: Party = await this.prismaService.party.findUnique({
      where: {
        id: partyId,
      },
    });
    if (party == null) throw new ApolloError('Party not found');
    if (party.status != PartyStatus.started)
      throw new ApolloError('Party status not corret');

    const newOrder = await this.prismaService.order.create({
      data: {
        status: OrderStatus.offer,
        price,
        comment,
        model: model as any,
        author: {
          connect: {
            id: party.authorId,
          },
        },
        music: {
          create: {
            label: music.label,
            name: music.name,
            url: music.url,
          },
        },
        user: {
          connect: {
            id: userId,
          },
        },
        party: {
          connect: {
            id: partyId,
          },
        },
      },
      include: {
        music: true,
      },
    });
    pubSub.publish('orderChanged', newOrder);
    return newOrder;
  }

  @Query(() => [Order])
  async getOrdersByPartyId(@Args('partyId') partyId: string): Promise<Order[]> {
    const orders = await this.prismaService.order.findMany({
      where: {
        partyId,
      },
      include: {
        music: true,
      },
    });
    return orders;
  }

  @UseGuards(PreAuthGuard)
  @Query(() => [Order])
  async getMyOrders(
    @Args('page', {
      type: () => Int,
      nullable: true,
      defaultValue: 0,
    })
    page: number = 0,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<Order[]> {
    const orders = await this.prismaService.order.findMany({
      where: {
        userId: userId,
      },
      orderBy: {
        id: 'desc',
      },
      include: {
        music: true,
      },
      skip: page,
      take: 20,
    });
    return orders;
  }
}
