import { Field, GraphQLISODateTime, InputType, Int } from '@nestjs/graphql';
import { Place } from 'src/custom_generator/place';
import GraphQLUpload from 'src/utils/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/utils/graphql_upload/Upload';

@InputType()
export class CreatePartyArg {
  @Field() name: string;

  @Field({
    nullable: true,
  })
  description?: string;

  @Field(() => Int) adult: number;

  @Field(() => [String], { defaultValue: [], nullable: true }) tags: string[];

  @Field(() => String, { nullable: true }) placeType: string;

  @Field(() => [String], { defaultValue: [], nullable: true })
  additionalTags: string[];

  @Field(() => [String], { defaultValue: [], nullable: true })
  typeTags: string[];

  @Field(() => String, { nullable: true }) partyType: string;

  @Field(() => [String], { defaultValue: [], nullable: true })
  folderIds: string[];

  @Field(() => GraphQLUpload, {
    nullable: true,
  })
  photo: Promise<FileUpload>;

  @Field(() => GraphQLISODateTime)
  dateStart: Date;

  @Field(() => GraphQLISODateTime)
  dateEnd: Date;

  @Field(() => Place)
  place: Place;
}
