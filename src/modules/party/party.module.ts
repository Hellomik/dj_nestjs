import { Module } from '@nestjs/common';
import { ClientPartyResolver } from './resolvers/client_party.resolver';
import { EventSubResolver } from './resolvers/event_sub.resolver';
import { SearchPostService } from './services/search_post.service';
import { CreatePartyResolver } from './resolvers/create.resolver';
import { AWSPhotoUploadModule } from '../uploadPhoto/awsSpace.module';
import { AuthorPartResolver } from './resolvers/author_party.resolver';
import { PaymentModule } from '../payment/payment.module';
import { PartyComingResolver } from './resolvers/party_coming.resolver';

@Module({
  imports: [AWSPhotoUploadModule, PaymentModule],
  providers: [
    ClientPartyResolver,
    EventSubResolver,
    SearchPostService,
    CreatePartyResolver,
    AuthorPartResolver,
    PartyComingResolver,
  ],
})
export class PartyModule {}
