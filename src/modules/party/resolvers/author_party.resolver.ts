import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { OrderStatus, PartyStatus } from '@prisma/client';
import { PartyCreateWithoutAuthorInput } from 'src/@generated/party/party-create-without-author.input';
import { Party } from 'src/@generated/party/party.model';
import { PayService } from 'src/modules/payment/services/pay_service';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';

@Resolver()
export class AuthorPartResolver {
  constructor(
    private prismaService: PrismaService,
    private payService: PayService,
  ) {}

  @UseGuards(PreAuthGuard)
  @Query(() => [Party])
  async getPartiesByAuthor(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ) {
    const parties = await this.prismaService.party.findMany({
      where: {
        author: {
          userId,
        },
        status: {
          notIn: [PartyStatus.canceled, PartyStatus.finished],
        },
      },
    });
    return parties;
  }

  @Mutation(() => Party)
  async cancelParty(@Args('id') id: string): Promise<Party> {
    const data = await this.prismaService.party.update({
      where: {
        id,
      },
      data: {
        status: PartyStatus.canceled,
      },
      include: {
        author: {
          include: {
            user: true,
          },
        },
      },
    });
    return data;
  }

  // @UseGuards(PreAuthGuard)
  // @Mutation(() => Party)
  // async createParty(
  //   @Args('partyData') partyCreateInput: PartyCreateWithoutAuthorInput,
  //   @CurrentTokenPayload() { id: authorId }: TokenPayload,
  // ): Promise<Party> {
  //   const createParty = await this.prismaService.party.create({
  //     data: {
  //       ...partyCreateInput,
  //       orders: {
  //         create: [],
  //       },
  //       author: {
  //         connect: {
  //           userId: authorId,
  //         },
  //       },
  //     },
  //   });
  //   return createParty;
  // }

  @Mutation(() => Party)
  async startParty(@Args('id') id: string): Promise<Party> {
    const data = await this.prismaService.party.update({
      where: {
        id,
      },
      data: {
        status: PartyStatus.started,
      },
      include: {
        author: {
          include: {
            user: true,
          },
        },
      },
    });
    return data;
  }

  @Mutation(() => Party)
  async finishParty(@Args('id') id: string): Promise<Party> {
    const party = await this.prismaService.party.update({
      where: {
        id,
      },
      data: {
        status: PartyStatus.finished,
      },
    });

    const orders = await this.prismaService.order.findMany({
      where: {
        status: OrderStatus.offer,
        partyId: id,
      },
    });

    for (let ind: number = 0; ind < orders.length; ++ind) {
      const dataFirst = orders[ind];
      if ((dataFirst.model as any)?.TransactionId != null) {
        await this.payService.cancelAuth({
          transactionId: dataFirst.model['TransactionId'],
        });
      }
    }

    this.prismaService.order.updateMany({
      where: {
        status: {
          not: OrderStatus.offer,
        },
        partyId: id,
      },
      data: {
        status: OrderStatus.finished,
      },
    });
    this.prismaService.order.updateMany({
      where: {
        status: OrderStatus.offer,
        partyId: id,
      },
      data: {
        status: OrderStatus.declined,
      },
    });
    return party;
  }
}
