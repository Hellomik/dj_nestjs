import { Args, GraphQLISODateTime, Query, Resolver } from '@nestjs/graphql';
import { PartyStatus } from '@prisma/client';
import { Party } from 'src/@generated/party/party.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import * as prismaClient from '@prisma/client';

@Resolver()
export class ClientPartyResolver {
  constructor(private prismaService: PrismaService) {}

  @Query(() => [Party])
  async getParties(
    @Args('city', {
      nullable: true,
    })
    city?: string,

    @Args('additionalTag', {
      nullable: true,
      type: () => [String],
    })
    additionalTag?: string[],

    @Args('placeType', {
      nullable: true,
    })
    placeType?: string,

    @Args('typeTag', {
      nullable: true,
      type: () => [String],
    })
    typeTag?: string[],

    @Args('dateStart', {
      nullable: true,
      type: () => GraphQLISODateTime,
    })
    dateStart?: Date,

    @Args('dateEnd', {
      nullable: true,
      type: () => GraphQLISODateTime,
    })
    dateEnd?: Date,
  ): Promise<Party[]> {
    const where: prismaClient.Prisma.PartyWhereInput = {};

    if (city != null) {
      where.city = city;
    }

    if (placeType != null) {
      where.placeType = placeType;
    }

    if (additionalTag != null) {
      where.additionalTag = {
        hasEvery: additionalTag,
      };
    }

    if (typeTag != null) {
      where.typeTag = {
        hasEvery: typeTag,
      };
    }

    if (dateStart != null && dateEnd != null) {
      where.dateStart = {
        gte: dateStart,
        lte: dateEnd,
      };
    }

    const data = await this.prismaService.party.findMany({
      where: {
        ...where,
        status: {
          notIn: [PartyStatus.canceled, PartyStatus.finished],
        },
      },
      include: {
        author: {
          include: {
            user: true,
          },
        },
      },
    });
    return data;
  }

  @Query(() => Party)
  async getPartyById(
    @Args('id')
    id?: string,
  ): Promise<Party> {
    const party = await this.prismaService.party.findUnique({
      where: {
        id,
      },
      include: {
        musicFolder: {
          include: {
            music: true,
          },
        },
        author: {
          include: {
            user: true,
          },
        },
        orders: {
          include: {
            music: true,
          },
        },
      },
    });
    return party;
  }
}
