import {
  Args,
  GraphQLISODateTime,
  Int,
  Mutation,
  Resolver,
} from '@nestjs/graphql';
import { Place } from 'src/custom_generator/place';
import { Party } from 'src/@generated/party/party.model';
import GraphQLUpload from 'src/utils/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/utils/graphql_upload/Upload';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { PrismaService } from 'src/modules/prisma/prisma.service';

import { UseGuards } from '@nestjs/common';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from 'src/modules/user/service/graphql_guard.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import { CreatePartyArg } from '../model/create_party_arg.model';
import { PhotoUrl } from 'src/modules/uploadPhoto/models/photo.model';

export const getFileExt = (filename: string): string => {
  return filename.slice(((filename.lastIndexOf('.') - 1) >>> 0) + 2);
};

@Resolver()
export class CreatePartyResolver {
  constructor(
    private readonly awsPhotoUploadService: AWSPhotoUploadService,
    private prismaService: PrismaService,
  ) {}
  //
  @UseGuards(PreAuthGuard)
  @Mutation(() => Party)
  async createParty(
    @Args('data') data: CreatePartyArg,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ) {
    let photoUrl: PhotoUrl;

    if (data.photo != null) {
      const dataPhoto = await data.photo;

      photoUrl = await this.awsPhotoUploadService.uploadImageAWS(
        dataPhoto.createReadStream(),
        getFileExt(dataPhoto.filename),
      );
    }

    const party = await this.prismaService.party.create({
      data: {
        city: 'Almaty',
        musicFolder: {
          connect: data.folderIds.map((id) => ({ id })),
        },
        dateEnd: data.dateEnd,
        dateStart: data.dateStart,
        name: data.name,
        typeTag: data.typeTags,
        placeType: data.placeType,
        additionalTag: data.additionalTags,
        description: data.description,
        place: data.place as any,

        author: {
          connect: {
            userId,
          },
        },
        photoUrl: photoUrl as any,
        tags: data.tags,
      },
    });
    return party;
  }
}
