import { Args, Mutation, Resolver, Subscription } from '@nestjs/graphql';
// import { OrderStatus } from '@prisma/client';
import { PubSub } from 'graphql-subscriptions';
import { Order } from 'src/@generated/order/order.model';
import { OrderStatus } from 'src/@generated/prisma/order-status.enum';
import {
  PrismaService,
  globalPrismaClient,
} from 'src/modules/prisma/prisma.service';

export type WithRequiredProperty<Type, Key extends keyof Type> = Type & {
  [Property in Key]-?: Type[Property];
};

export type OrderWithMusic = WithRequiredProperty<Order, 'music'>;

export const pubSub: CustomPubSub<
  OrderWithMusic,
  'orderChanged' | 'orderListenAsAuthor'
> = new PubSub();

export interface CustomPubSub<T, S extends string> extends PubSub {
  publish(triggerName: S, payload: T): Promise<void>;
}

@Resolver()
export class EventSubResolver {
  constructor(private prismaService: PrismaService) {}

  @Subscription(() => Order, {
    filter: (
      payload: OrderWithMusic,
      { partyId }: { partyId: string },
      { userId, authorId }: { userId: string; authorId?: string },
    ) => {
      pubSub.publish('orderListenAsAuthor', payload);
      return (
        payload.authorId == authorId ||
        userId == payload.userId ||
        partyId == payload.partyId
      );
    },
    resolve: (payload: OrderWithMusic) => {
      return payload;
    },
  })
  orderChanged(@Args('partyId', { nullable: true }) partyId?: string) {
    return pubSub.asyncIterator('orderChanged');
  }

  @Subscription(() => Order, {
    filter: (
      payload: OrderWithMusic,
      {},
      { authorId }: { userId: string; authorId?: string },
    ) => {
      return payload.authorId == authorId;
    },
    resolve: (payload: OrderWithMusic) => {
      return payload;
    },
  })
  orderListenAsAuthor() {
    return pubSub.asyncIterator('orderListenAsAuthor');
  }

  // @Mutation(() => Boolean)
  // async orderToListend(
  //   @Args('id')
  //   id: string,
  //   @Args('price')
  //   price: number,
  //   @Args('priority')
  //   priority: number,
  //   @Args('status', { type: () => OrderStatus })
  //   status: OrderStatus,
  //   @Args('authorId')
  //   authorId: string,
  //   @Args('musicId')
  //   musicId: string,
  //   @Args('userId')
  //   userId: string,
  //   @Args('partyId')
  //   partyId: string,
  // ) {
  //   await pubSub.publish('orderChanged', {
  //     id,
  //     price,
  //     priority,
  //     status,
  //     authorId,
  //     musicId,
  //     userId,
  //     partyId,
  //     acceptTime: null,
  //     startTime: null,
  //     finishTime: null,
  //   });
  //   return true;
  // }
}
