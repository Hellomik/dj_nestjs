import { UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { TokenPayload } from 'src/modules/user/model/tokenPayload.model';
import {
  CurrentTokenPayload,
  PreAuthGuard,
  PreAuthGuardLight,
} from 'src/modules/user/service/graphql_guard.service';

@Resolver()
export class PartyComingResolver {
  constructor(readonly prismaService: PrismaService) {}

  @Query(() => Int)
  async getPartyComingCount(@Args('partyId') partyId: string): Promise<number> {
    const countParty = await this.prismaService.partyComing.count({
      where: {
        partyId,
      },
    });
    return countParty;
  }

  @UseGuards(PreAuthGuardLight)
  @Query(() => Boolean, {
    nullable: true,
  })
  async amIGo(
    @CurrentTokenPayload() body: TokenPayload,
    @Args('partyId') partyId: string,
  ): Promise<boolean | null> {
    if (body?.id == undefined) return null;
    const userId: string = body?.id;
    const founded = await this.prismaService.partyComing.findFirst({
      where: {
        userId,
        partyId: partyId,
      },
    });
    return founded != null;
    // return true;
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => Boolean)
  async iGo(
    @CurrentTokenPayload() body: TokenPayload,
    @Args('partyId') partyId: string,
  ) {
    const userId: string = body?.id;
    const founded = await this.prismaService.partyComing.findFirst({
      where: {
        userId,
        partyId,
      },
    });
    if (founded != null) return true;
    const created = await this.prismaService.partyComing.create({
      data: {
        partyId,
        userId,
      },
    });
    return true;
  }
}
