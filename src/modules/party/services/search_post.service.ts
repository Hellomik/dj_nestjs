import { Inject, Injectable } from '@nestjs/common';
import MeiliSearch from 'meilisearch';
import { Db } from 'mongodb';
import { InjectMeiliSearch } from 'nestjs-meilisearch';
import { PrismaService } from 'src/modules/prisma/prisma.service';

@Injectable()
export class SearchPostService {
  constructor(
    @Inject('DATABASE_CONNECTION') private database: Db,
    @InjectMeiliSearch() private readonly meiliSearch: MeiliSearch,
    private prismaService: PrismaService,
  ) {
    this.init();
  }

  async init() {
    try {
      const parties = await this.prismaService.party.findMany({
        include: {
          author: {
            include: {
              user: true,
            },
          },
        },
      });
      await this.meiliSearch.index('party').updateDocuments(parties, {
        primaryKey: 'id',
      });
    } catch (e) {
      console.log(e);
    }
  }
}
