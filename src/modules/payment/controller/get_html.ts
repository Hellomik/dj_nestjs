import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { UserSaveCardService } from 'src/modules/user/service/user_save_card.service';

const getHtmlFile = async (userId: string) => `
<html>
  <head>
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments.js"></script>
    <script src="https://widget.cloudpayments.ru/bundles/paymentblocks.js"></script>
  </head>
  <body>
    <div id="element"></div>

    <script>
      var blocksApp = new cp.PaymentBlocks(
        {
          publicId: 'pk_846a1cd285ac5375a0e7016ef8d95',
          description: 'Сохранение карты',
          amount: 100,
          currency: 'KZT',
          accountId: '${userId}',
          requireEmail: true,
          language: 'ru-RU',
        },
        {
          appearance: {
            colors: {
              primaryButtonColor: '#2E71FC',
              primaryButtonTextColor: '#FFFFFF',
              primaryHoverButtonColor: '#2E71FC',
              primaryButtonHoverTextColor: '#FFFFFF',
              activeInputColor: '#0B1E46',
              inputBackground: '#FFFFFF',
              inputColor: '#8C949F',
              inputBorderColor: '#E2E8EF',
              errorColor: '#EB5757',
            },
            borders: {
              radius: '8px',
            },
          },
          components: {
            paymentButton: {
              text: 'Оплатить',
              fontSize: '16px',
            },
            paymentForm: {
              labelFontSize: '16px',
              activeLabelFontSize: '12px',
              fontSize: '16px',
            },
          },
        },
      );
      // blocksApp.mount(document.getElementById("element"));
      blocksApp.on('success', (result) => {
        window.open('https://payment.mynight.kz/success', '_self');
        console.log('success', result); // WidgetResult
      });

      // Подписка на событие ошибки оплаты
      blocksApp.on('fail', (result) => {
        console.log('fail', result); // WidgetResult
      });

      // Подписка на события разрушения платежного блока
      blocksApp.on('destroy', () => {
        console.log('destroy');
      });
      const pay = function () {
        var widget = new cp.CloudPayments();
        widget.pay(
          'auth', // или 'charge'
          {
            //options
            publicId: 'pk_846a1cd285ac5375a0e7016ef8d95', //id из личного кабинета
            description: 'Инициализация', //назначение
            amount: 100, //сумма
            currency: 'KZT', //валюта
            accountId: '${userId}', //идентификатор плательщика (необязательно)
            data: {
              myProp: 'myProp value',
            },
          },
          {
            onSuccess: function (options) {
              // success

              window.open('https://payment.mynight.kz/success', '_self');
              //действие при успешной оплате
            },
            onFail: function (reason, options) {
              console.log('FAIL');
              window.open('https://payment.mynight.kz/fail', '_self');
              // fail
              //действие при неуспешной оплате
            },
            onComplete: function (paymentResult, options) {
              //Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции.
              //например вызов вашей аналитики Facebook Pixel
            },
          },
        );
      };
      pay();
    </script>
  </body>
</html>
`;

@Controller('get_payment')
export class GetHTMLController {
  constructor(private userSaveCardService: UserSaveCardService) {}

  @Get()
  async getPayment(@Query('userId') userId: string) {
    if (!userId) throw 'No data';
    console.log(userId);
    return getHtmlFile(userId);
  }

  @Post('confirm')
  async postConfirm(@Body() body) {
    this.userSaveCardService.saveCard({
      transactionId: body['TransactionId'],
      accountId: body['AccountId'],
      cardLastFour: body['CardLastFour'],
      cardType: body['CardType'],
      token: body['Token'],
    });

    return { code: 0 };
  }
}
