import { Module, forwardRef } from '@nestjs/common';
import { GetHTMLController } from './controller/get_html';
import { UserModule } from '../user/user.module';
import { PayService } from './services/pay_service';
import { HttpModule } from '@nestjs/axios';
import { RemovePaymentResolver } from './resolvers/remove_payment.resolver';

@Module({
  imports: [forwardRef(() => UserModule), HttpModule],
  controllers: [GetHTMLController],
  providers: [PayService, RemovePaymentResolver],
  exports: [PayService],
})
export class PaymentModule {}
