import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { UserSaveCardService } from 'src/modules/user/service/user_save_card.service';

@Resolver()
export class RemovePaymentResolver {
  constructor(private userSaveCardService: UserSaveCardService) {}

  @Mutation(() => Boolean)
  async removePaymentById(
    @Args('paymentId') paymentId: string,
  ): Promise<boolean> {
    await this.userSaveCardService.deleteCard(new ObjectId(paymentId));
    return true;
  }
}
