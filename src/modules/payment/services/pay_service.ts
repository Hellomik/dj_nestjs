import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';

var username = 'pk_846a1cd285ac5375a0e7016ef8d95';
var password = '9d7019106d0f02ad93089ddf0929cb55';

var auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');

@Injectable()
export class PayService {
  constructor(private httpService: HttpService) {}

  async payAuth(__: {
    amount: number;
    accountId: string;
    token: string;
  }): Promise<Object> {
    const body = {
      Amount: __.amount,
      Currency: 'KZT',
      AccountId: __.accountId,
      Token: __.token,
    };

    const responce = await this.httpService.axiosRef.post(
      'https://api.cloudpayments.kz/payments/tokens/auth',
      body,
      {
        headers: {
          Authorization: auth,
        },
      },
    );
    if (responce.data['Success'] == true) return responce.data['Model'];
    throw responce.data['Message'];
  }
  async confirmAuth(__: {
    transactionId: string;
    amount: number;
  }): Promise<string> {
    const body = {
      TransactionId: __.transactionId,
      Amount: __.amount,
    };
    const responce = await this.httpService.axiosRef.post(
      'https://api.cloudpayments.kz/payments/confirm',
      body,
      {
        headers: {
          Authorization: auth,
        },
      },
    );
    if (responce.data['Success'] == true) return __.transactionId;
    throw responce.data['Message'];
  }
  async cancelAuth(__: { transactionId: string }) {
    const body = {
      TransactionId: __.transactionId,
    };

    const responce = await this.httpService.axiosRef.post(
      'https://api.cloudpayments.kz/payments/void',
      body,
      {
        headers: {
          Authorization: auth,
        },
      },
    );
    console.log(responce.data);
  }
}
