import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import * as TelegramBot from 'node-telegram-bot-api';
import { TelegramModule } from '../telegram.module';
import { CodeService } from 'src/modules/user/service/code.service';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { ConfigService } from '@nestjs/config';

const accessTokenTelegram =
  process.env.prod == '1'
    ? '6093977564:AAEBN9b3lQFe_NcmSstz3KiJCR7ZR4ArwMg'
    : '6080922474:AAFYWCYA--pZJGHUXAA_9-InBuVJtCRaZ78';

export const telegramBot = new TelegramBot(accessTokenTelegram, {
  polling: process.env.prod == '1' ? true : false,
});

@Injectable()
export class InitializeService {
  telegramCollection: Collection<{
    _id: ObjectId;
    phoneNumber: string;
    chatId: number;
  }>;

  constructor(
    @Inject('DATABASE_CONNECTION') private database: Db,
    private codeSerivce: CodeService,
    private configService: ConfigService,
  ) {
    const isProd = this.configService.get('prod');
    // console.log(accessTokenTelegram);
    this.telegramCollection = isProd
      ? this.database.collection('telegrambot')
      : this.database.collection('telegrambotTest');
    this.init();
  }

  async onMessage(
    message: TelegramBot.Message,
    metadata: TelegramBot.Metadata,
  ) {
    try {
      if (message.text == '/start') {
        this.setKeyboard(message, metadata);
        return;
      }
      if (metadata.type == 'contact') {
        if (message.contact.user_id != message.from.id) {
          telegramBot.sendMessage(message.chat.id, `Это не ваш номер телефона`);
          return;
        }
        const phoneNumber = cleanPhoneNumber(message.contact.phone_number);
        await this.telegramCollection.updateOne(
          {
            phoneNumber,
          },
          {
            $set: {
              phoneNumber,
              chatId: message.chat.id,
            },
          },
          { upsert: true },
        );
        const verifyCode = this.codeSerivce.generateCode(phoneNumber);
        telegramBot
          .sendMessage(
            message.chat.id,
            `Можете продолжить верификацию в приложении.\nВаш код верификации ${verifyCode} для номера +${phoneNumber}`,
          )
          .catch((e) => console.log(e));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async setKeyboard(
    message: TelegramBot.Message,
    metadata: TelegramBot.Metadata,
  ) {
    try {
      await telegramBot.sendMessage(
        message.chat.id,
        'Как мы можем с вами связаться ?',
        {
          parse_mode: 'Markdown',
          reply_markup: {
            one_time_keyboard: true,
            keyboard: [
              [
                {
                  text: 'Номер телефона',
                  request_contact: true,
                },
              ],
            ],
          },
        },
      );
    } catch (e) {
      console.log(e);
    }
  }

  async init() {
    telegramBot.on(
      'message',
      (message: TelegramBot.Message, metadata: TelegramBot.Metadata) =>
        this.onMessage(message, metadata),
    );
  }
}
