import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { CodeService } from 'src/modules/user/service/code.service';
import { telegramBot } from './initialize_service';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SendCodeTelegramService {
  telegramCollection: Collection<{
    _id: ObjectId;
    phoneNumber: string;
    chatId: number;
  }>;

  constructor(
    @Inject('DATABASE_CONNECTION') private database: Db,
    private codeSerivce: CodeService,
    private configService: ConfigService,
  ) {
    const isProd = this.configService.get('prod');
    this.telegramCollection = isProd
      ? this.database.collection('telegrambot')
      : this.database.collection('telegrambotTest');
  }

  async sendCode(__: { phoneNumber: string }): Promise<boolean> {
    const phoneNumber = cleanPhoneNumber(__.phoneNumber);
    const entity = await this.telegramCollection.findOne({
      phoneNumber: phoneNumber,
    });
    if (entity == null) throw 'Phone number not found';
    const verifyCode = this.codeSerivce.generateCode(phoneNumber);
    telegramBot.sendMessage(
      entity.chatId,
      `Можете продолжить верификацию в приложении.\nВаш код верификации ${verifyCode} для номера +${phoneNumber}`,
    );
    return true;
  }
}
