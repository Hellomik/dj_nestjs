import { HttpModule } from '@nestjs/axios';
import { Module, forwardRef } from '@nestjs/common';
import { InitializeService } from './services/initialize_service';
import { UserModule } from '../user/user.module';
import { SendCodeTelegramService } from './services/send_code.service.tg';

@Module({
  imports: [HttpModule, forwardRef(() => UserModule)],
  providers: [InitializeService, SendCodeTelegramService],
  exports: [SendCodeTelegramService],
})
export class TelegramModule {}
