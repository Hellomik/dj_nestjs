import { Field, InputType, ObjectType } from '@nestjs/graphql';
import GraphQLUpload from 'src/utils/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/utils/graphql_upload/Upload';

export interface PhotoUrl {
  xLarge: string;
  large: string;
  little: string;
}

@InputType('PhotoUrlInput')
export class PhotoUrlInput implements PhotoUrl {
  @Field()
  xLarge: string;

  @Field()
  large: string;

  @Field()
  little: string;
}

@InputType('AddPhoto')
export class AddPhoto {
  @Field(() => GraphQLUpload)
  photo: Promise<FileUpload>;
}

@InputType('RemovePhoto')
export class RemovePhoto {
  @Field(() => PhotoUrlInput)
  photoUrl: PhotoUrl;
}

@InputType('EditPhoto')
export class EditPhotos {
  @Field(() => [AddPhoto], { nullable: true })
  addPhoto?: AddPhoto[];

  @Field(() => [RemovePhoto], { nullable: true })
  removePhoto?: RemovePhoto[];
}
