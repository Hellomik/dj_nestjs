import { Controller, Get, Query } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/modules/prisma/prisma.service';

const htmlResponce: string = `<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Verification Page</title>
    <style>
      body {
        background-color: #0f0f0f;
        color: #ffffff;
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
      }
      .container {
        text-align: center;
      }
      h1 {
        color: #d39800;
        margin-bottom: 20px;
      }
      .message {
        color: #ffffff;
        font-size: 24px;
        margin-bottom: 20px;
      }
      .continue-button {
        background-color: #d39800;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 18px;
        cursor: pointer;
        border-radius: 5px;
        text-decoration: none;
      }
      .continue-button:hover {
        background-color: #b88600;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>My Night</h1>
      <div class="message">
        Your account verified. Continue in mobile application.
      </div>
    </div>
  </body>
</html>
`;

@Controller('verification')
export class AuthVetificationController {
  constructor(
    private jwtService: JwtService,
    private prismaService: PrismaService,
  ) {}

  @Get()
  async verificationResponce() {
    const token = '';
    const obj = this.jwtService.verify(token);
    const found = this.prismaService.user.findFirst({
      where: {
        email: obj.email,
      },
    });
    if (found == null) throw 'No Account';
    await this.prismaService.user.update({
      where: {
        email: obj.email,
      },
      data: {
        emailVerified: true,
      },
    });

    return htmlResponce;
  }
}
