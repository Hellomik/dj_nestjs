import { ObjectId } from 'mongodb';

export interface PaymentTokenModel {
  _id: ObjectId;
  token: string;
  cardType: string;
  cardLastFour: string;
  transactionId: string;
}
