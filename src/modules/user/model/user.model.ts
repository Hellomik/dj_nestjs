import { ObjectId } from 'mongodb';

export interface UserModel {
  _id: ObjectId;
  email: string;
  hash: string;
}
