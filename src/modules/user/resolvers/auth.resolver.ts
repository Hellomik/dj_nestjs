import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ApolloError } from 'apollo-server-express';
import * as TokenGraph from 'src/@generated/token/token.model';
import { AuthService } from '../service/auth.service';
import { Collection, Db, ObjectId } from 'mongodb';
import { Inject, UseGuards } from '@nestjs/common';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { SendCodeTelegramService } from 'src/modules/telegram/services/send_code.service.tg';
import { CodeService } from '../service/code.service';
import { SendCodeWaService } from 'src/modules/whatsapp/services/send_code_wa.service';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from '../service/graphql_guard.service';
import { TokenPayload } from '../model/tokenPayload.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';

@Resolver()
export class AuthResolver {
  telegramCollection: Collection<{
    _id: ObjectId;
    phoneNumber: string;
    chatId: number;
  }>;

  constructor(
    @Inject('DATABASE_CONNECTION') private database: Db,
    private authService: AuthService,
    private codeService: CodeService,
    private sendCodeWaService: SendCodeWaService,
    private sendCodeTelegramService: SendCodeTelegramService,
    private prismaService: PrismaService,
  ) {
    this.telegramCollection = this.database.collection('telegrambot');
  }

  @Mutation(() => TokenGraph.Token)
  async registerUser(
    @Args('email') email: string,
    @Args('password') password: string,
    @Args('device') device: string,
    // @Args('name') name: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
  ): Promise<TokenGraph.Token> {
    try {
      const token = await this.authService.registerByEmail({
        email,
        password,
        device,
        fcmToken,
        // name,
      });
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @Mutation(() => Boolean)
  async updateUserDetails(
    @Args('email') email: string,
    @Args('phoneNumber') phoneNumber: string,
    // @Args('name', @) name: string,
    @Args('password') password: string,
  ): Promise<boolean> {
    try {
      const isUpdated = await this.authService.editUserDetails(
        email,
        phoneNumber,
        // name,
        password,
      );
      return isUpdated;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @Mutation(() => TokenGraph.Token)
  async loginUser(
    @Args('email') _email: string,
    @Args('password') password: string,
    @Args('device') device: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
  ): Promise<TokenGraph.Token> {
    try {
      const email = _email.toLowerCase();
      const token = await this.authService.login({
        email,
        password,
        device,
        fcmToken,
      });
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => Boolean)
  async uploadToken(
    @Args('device') device: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<boolean> {
    console.log(device);
    console.log(fcmToken);
    await this.prismaService.token.upsert({
      where: {
        userIdDevice: {
          device,
          userId,
        },
      },
      create: {
        fcmToken,
        device,
        userId,
      },
      update: {
        fcmToken,
      },
    });
    return true;
  }

  @Mutation(() => Boolean)
  async sendCodeTelegram(
    @Args('phoneNumber') _phoneNumber: string,
  ): Promise<boolean> {
    const phoneNumber = cleanPhoneNumber(_phoneNumber);
    const entity = await this.telegramCollection.findOne({
      phoneNumber,
    });
    if (entity == null) return false;
    this.sendCodeTelegramService
      .sendCode({
        phoneNumber,
      })
      .catch((e) => {
        console.log(e);
      });
    return true;
  }

  @Mutation(() => Boolean)
  async sendCodeWhatsApp(
    @Args('phoneNumber') _phoneNumber: string,
  ): Promise<boolean> {
    const phoneNumber = cleanPhoneNumber(_phoneNumber);
    const code = this.codeService.generateCode(phoneNumber);
    await this.sendCodeWaService.sendCode({
      phoneNumber,
      code,
    });
    return true;
  }

  @Mutation(() => TokenGraph.Token)
  async verifyCodeRegister(
    @Args('phoneNumber') phoneNumber: string,
    @Args('device') device: string,
    @Args('name') name: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
    @Args('code') code: string,
  ): Promise<TokenGraph.Token> {
    try {
      this.codeService.verifyCode({
        phoneNumber,
        code,
      });
      const token = await this.authService.registerByPhoneNumber({
        phoneNumber,
        device,
        fcmToken,
        name,
      });
      this.codeService.cleanPhoneNumber(phoneNumber);
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @Mutation(() => TokenGraph.Token)
  async verifyCodeLogin(
    @Args('phoneNumber') phoneNumber: string,
    @Args('device') device: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
    @Args('code') code: string,
  ): Promise<TokenGraph.Token> {
    try {
      this.codeService.verifyCode({
        phoneNumber,
        code,
      });
      const token = await this.authService.loginByPhoneNumber({
        phoneNumber,
        device,
        fcmToken,
      });
      this.codeService.cleanPhoneNumber(phoneNumber);
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @Mutation(() => TokenGraph.Token)
  async fastLogin(
    @Args('email') email: string,
    @Args('password') password: string,
    @Args('device') device: string,

    @Args('fcmToken', { nullable: true }) fcmToken: string,
  ): Promise<TokenGraph.Token> {
    try {
      const token = await this.authService.fastLoginEmail({
        email,
        password,
        device,
        fcmToken,
      });
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }

  @Mutation(() => TokenGraph.Token)
  async fastLoginCodeRegister(
    @Args('phoneNumber') phoneNumber: string,
    @Args('device') device: string,
    @Args('fcmToken', { nullable: true }) fcmToken: string,
    @Args('code') code: string,
  ): Promise<TokenGraph.Token> {
    try {
      this.codeService.verifyCode({
        phoneNumber,
        code,
      });
      const token = this.authService.fastLoginCodeRegister({
        device,
        fcmToken,
        phoneNumber,
      });

      this.codeService.cleanPhoneNumber(phoneNumber);
      return token;
    } catch (e) {
      throw new ApolloError(e.toString());
    }
  }
}
