import { UseGuards } from '@nestjs/common';
import { Query, Mutation, Resolver } from '@nestjs/graphql';
import * as ggraph from 'src/@generated/author/author.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from '../service/graphql_guard.service';
import { TokenPayload } from '../model/tokenPayload.model';

@Resolver()
export class AuthorResolver {
  constructor(private prismaService: PrismaService) {}

  @UseGuards(PreAuthGuard)
  @Mutation(() => ggraph.Author)
  async createAuthor(@CurrentTokenPayload() { id: userId }: TokenPayload) {
    const author = await this.prismaService.author.create({
      data: {
        isApproved: true,
        user: {
          connect: {
            id: userId,
          },
        },
      },
    });

    return author;
  }
}
