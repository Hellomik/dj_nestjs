import { Resolver, Query, Args } from '@nestjs/graphql';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from '../service/graphql_guard.service';
import { Inject, UseGuards } from '@nestjs/common';
import { PaymentToken } from 'src/custom_generator/payment_token';
import { TokenPayload } from '../model/tokenPayload.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { PaymentTokenModel } from '../model/payment_token.model';
import { Collection, Db, ObjectId } from 'mongodb';

@Resolver()
export class UserPaymentResolver {
  userCollection: Collection<{
    _id: ObjectId;
    paymentToken: PaymentTokenModel[];
  }>;
  constructor(
    private prismaService: PrismaService,
    @Inject('DATABASE_CONNECTION') private database: Db,
  ) {
    this.userCollection = this.database.collection('User');
  }

  @UseGuards(PreAuthGuard)
  @Query(() => [PaymentToken])
  async getPayments(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<PaymentToken[]> {
    const userFound = await this.userCollection.findOne({
      _id: new ObjectId(userId),
    });
    if (userFound == null) throw 'User not found';
    // const userFound = await this.prismaService.user.findUnique({
    //   where: {
    //     id: userId,
    //   },
    // });
    const paymentTokens: PaymentTokenModel[] = userFound.paymentToken ?? [];
    return paymentTokens.map<PaymentToken>((e: PaymentTokenModel) => {
      return {
        cardLastFour: e.cardLastFour,
        cardType: e.cardType,
        id: e._id.toHexString(),
      };
    });
  }

  @UseGuards(PreAuthGuard)
  @Query(() => [PaymentToken])
  async removePayment(
    @Args('id') id: string,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<PaymentToken[]> {
    const userFound = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    const paymentTokens: PaymentTokenModel[] = (userFound.paymentToken ??
      []) as any[] as PaymentTokenModel[];
    const newPaymentsTokens = paymentTokens.filter(
      (val) => val._id.toHexString() == id,
    );
    await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        paymentToken: newPaymentsTokens as any[],
      },
    });
    return newPaymentsTokens.map<PaymentToken>((e: PaymentTokenModel) => {
      return {
        cardLastFour: e.cardLastFour,
        cardType: e.cardType,
        id: e._id.toHexString(),
      };
    });
  }
}
