import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { User } from 'src/@generated/user/user.model';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import {
  CurrentTokenPayload,
  PreAuthGuard,
} from '../service/graphql_guard.service';
import { UseGuards } from '@nestjs/common';
import { TokenPayload } from '../model/tokenPayload.model';
import GraphQLUpload from 'src/utils/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/utils/graphql_upload/Upload';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { getFileExt } from 'src/modules/party/resolvers/create.resolver';
import { PhotoUrl } from 'src/modules/uploadPhoto/models/photo.model';

@Resolver()
export class UserResolver {
  constructor(
    private readonly awsPhotoUploadService: AWSPhotoUploadService,
    private prismaService: PrismaService,
  ) {}

  @UseGuards(PreAuthGuard)
  @Query(() => User)
  async getUserByToken(
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<User> {
    return this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
  }

  @UseGuards(PreAuthGuard)
  @Mutation(() => User)
  async uploadPhotoUser(
    @Args('photo', { type: () => GraphQLUpload, nullable: true })
    photo: Promise<FileUpload>,
    @CurrentTokenPayload() { id: userId }: TokenPayload,
  ): Promise<User> {
    let photoUrl: PhotoUrl | null = null;
    if (photo) {
      const dataPhoto = await photo;
      photoUrl = await this.awsPhotoUploadService.uploadImageAWS(
        dataPhoto.createReadStream(),
        getFileExt(dataPhoto.filename),
      );
    }
    return this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        photoUrl: photoUrl as any,
      },
    });
  }
}
