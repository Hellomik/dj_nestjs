import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import {
  emailFormat,
  privateRefreshKey,
  saltOrRounds,
} from 'src/utils/jwt_keys';
import * as bcrypt from 'bcrypt';
import { Token } from '@prisma/client';
import { SendVerificationService } from './send_verification.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private prismaService: PrismaService,
    private sendVerificationService: SendVerificationService,
  ) {}

  async loginByPhoneNumber(__: {
    phoneNumber: string;

    device: string;
    fcmToken?: string;
  }): Promise<Token> {
    const { phoneNumber, device, fcmToken } = __;
    const user = await this.prismaService.user.findFirst({
      where: {
        phoneNumber,
      },
    });

    const accessToken = this.jwtService.sign({
      id: user.id,
    });
    const refreshToken = this.jwtService.sign(
      {
        id: user.id,
      },
      {
        privateKey: privateRefreshKey,
        secret: '123456789',
        expiresIn: '100d',
      },
    );
    return await this.prismaService.token.upsert({
      where: {
        userIdDevice: {
          userId: user.id,
          device: device,
        },
      },
      update: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
      },
      create: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
        userId: user.id,
      },
    });
  }

  async registerByPhoneNumber(__: {
    phoneNumber: string;

    device: string;
    fcmToken?: string;
    name: string;
  }): Promise<Token> {
    const { phoneNumber, device, fcmToken, name } = __;
    const user = await this.prismaService.user.findFirst({
      where: {
        OR: [
          {
            phoneNumber,
          },
          {
            name,
          },
        ],
      },
    });
    if (user != null) throw 'User exist';

    const addedUser = await this.prismaService.user.create({
      data: {
        name,
        phoneNumber,
        // hash: await bcrypt.hash(password, saltOrRounds),
      },
    });

    const accessToken = this.jwtService.sign({
      id: addedUser.id,
    });
    const refreshToken = this.jwtService.sign(
      {
        id: addedUser.id,
      },
      {
        privateKey: privateRefreshKey,
        secret: '123456789',
        expiresIn: '100d',
      },
    );
    return await this.prismaService.token.create({
      data: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
        userId: addedUser.id,
      },
    });
  }

  async registerByEmail(__: {
    email: string;
    password: string;
    device: string;
    fcmToken?: string;
    // name: string;
  }): Promise<Token> {
    const { email: emailNotClean, password, device, fcmToken } = __;
    const email = emailNotClean.toLowerCase();
    if (!email.match(emailFormat)) throw 'Email is wrong';

    const user = await this.prismaService.user.findFirst({
      where: {
        OR: [
          {
            email,
          },
        ],
      },
    });

    if (user != null) throw 'User exist';

    const addedUser = await this.prismaService.user.create({
      data: {
        // name,
        email,
        hash: await bcrypt.hash(password, saltOrRounds),
      },
    });

    const accessToken = this.jwtService.sign({
      id: addedUser.id,
    });
    const refreshToken = this.jwtService.sign(
      {
        id: addedUser.id,
      },
      {
        privateKey: privateRefreshKey,
        secret: '123456789',
        expiresIn: '100d',
      },
    );
    return await this.prismaService.token.create({
      data: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
        userId: addedUser.id,
      },
    });
  }
  async editUserDetails(
    email: string,

    phoneNumber?: string,
    // name?: string,
    password?: string,
  ): Promise<boolean> {
    if (!email && !phoneNumber) {
      throw new Error(
        'At least one field (email, phoneNumber, name) must be provided for the update.',
      );
    }

    const user = await this.prismaService.user.findUnique({
      where: {
        email: email,
      },
    });

    if (!user) {
      throw new Error('User not found');
    }

    const passwordMatch = await bcrypt.compare(password, user.hash);

    if (!passwordMatch) {
      throw new Error('Password didnt match');
    }

    if (email) {
      user.email = email;
    }

    if (phoneNumber) {
      user.phoneNumber = phoneNumber;
    }

    // if (name) {
    //   user.name = name;
    // }

    const updatedUser = await this.prismaService.user.update({
      where: {
        email: email,
      },
      data: {
        email: user.email,
        phoneNumber: user.phoneNumber,
        // name: user.name,
      },
    });

    return !!updatedUser;
  }

  async login(__: {
    email: string;
    password: string;
    device: string;
    fcmToken?: string;
  }): Promise<Token> {
    const { email: emailNotClean, password, device, fcmToken } = __;
    const email = emailNotClean.toLowerCase();
    const user = await this.prismaService.user.findFirst({
      where: {
        email,
      },
    });
    if (!user) throw 'User not found';
    if (user.emailVerified != true) throw 'User not verified';

    const comapre = await bcrypt.compare(password, user.hash);
    if (!comapre) throw "Password didn't match";
    const accessToken = this.jwtService.sign({
      id: user.id,
    });
    const refreshToken = this.jwtService.sign(
      {
        id: user.id,
      },
      {
        privateKey: privateRefreshKey,
        secret: '123456789',
        expiresIn: '100d',
      },
    );

    const token = await this.prismaService.token.upsert({
      create: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
        userId: user.id,
      },
      update: {
        accessToken,
        refreshToken,
      },
      where: {
        userIdDevice: {
          device,
          userId: user.id,
        },
      },
    });

    return token;
  }

  async fastLoginEmail(__: {
    email: string;
    password: string;
    device: string;
    fcmToken?: string;
  }): Promise<Token> {
    const { email: emailNotClean, password, device, fcmToken } = __;
    const email = emailNotClean.toLowerCase();
    if (!email.match(emailFormat)) throw 'Email is wrong';

    const user = await this.prismaService.user.findFirst({
      where: {
        OR: [
          {
            email,
          },
        ],
      },
    });

    if (user != null && user.emailVerified) {
      return this.login(__);
    }

    if (user != null) {
      await this.prismaService.user.update({
        where: { email },
        data: {
          hash: await bcrypt.hash(password, saltOrRounds),
        },
      });
    } else {
      await this.prismaService.user.create({
        data: {
          email,
          hash: await bcrypt.hash(password, saltOrRounds),
        },
      });
    }
    this.sendVerificationService.sendEmail(email);
    throw 'Verify user';
  }
  async fastLoginCodeRegister(__: {
    phoneNumber: string;
    device: string;
    fcmToken: string;
  }): Promise<Token> {
    const { phoneNumber, device, fcmToken } = __;
    const user = await this.prismaService.user.findFirst({
      where: {
        OR: [
          {
            phoneNumber,
          },
        ],
      },
    });
    if (user != null) {
      const addedUser = user;

      const accessToken = this.jwtService.sign({
        id: addedUser.id,
      });
      const refreshToken = this.jwtService.sign(
        {
          id: addedUser.id,
        },
        {
          privateKey: privateRefreshKey,
          secret: '123456789',
          expiresIn: '100d',
        },
      );
      return await this.prismaService.token.create({
        data: {
          accessToken,
          refreshToken,
          device,
          fcmToken,
          userId: addedUser.id,
        },
      });
    }

    const addedUser = await this.prismaService.user.create({
      data: {
        phoneNumber,
      },
    });

    const accessToken = this.jwtService.sign({
      id: addedUser.id,
    });
    const refreshToken = this.jwtService.sign(
      {
        id: addedUser.id,
      },
      {
        privateKey: privateRefreshKey,
        secret: '123456789',
        expiresIn: '100d',
      },
    );
    return await this.prismaService.token.create({
      data: {
        accessToken,
        refreshToken,
        device,
        fcmToken,
        userId: addedUser.id,
      },
    });
  }
}
