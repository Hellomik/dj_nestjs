import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { generateMailer, gmailTransporter } from 'src/utils/nodemailer';

@Injectable()
export class SendVerificationService {
  constructor(private jwtService: JwtService) {}

  async sendEmail(email: string): Promise<void> {
    const token = this.jwtService.sign({ email });
    const url = `https://testdj.gacademy.studio/verification?token=${token}`;

    await gmailTransporter.sendMail(generateMailer(email, url));
  }
}
