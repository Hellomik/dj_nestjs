import { Inject, Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { PaymentTokenModel } from '../model/payment_token.model';
import { Collection, Db, ObjectId } from 'mongodb';

@Injectable()
export class UserSaveCardService {
  userCollection: Collection<{
    paymentToken: PaymentTokenModel[];
  }>;

  constructor(
    private prismaService: PrismaService,
    @Inject('DATABASE_CONNECTION') private database: Db,
  ) {
    this.userCollection = this.database.collection('User');
  }

  async saveCard(__: {
    transactionId: string;
    token: string;
    accountId: string;
    cardType: string;
    cardLastFour: string;
  }): Promise<User> {
    try {
      const needToAdd: PaymentTokenModel = {
        _id: new ObjectId(),
        token: __.token,
        cardType: __.cardType,
        cardLastFour: __.cardLastFour,
        transactionId: __.transactionId,
      };
      
      const foubndedCardUser = await this.userCollection.findOne({
        _id: new ObjectId(__.accountId),
        paymentToken: needToAdd,
      });
      if (foubndedCardUser != null) return;
      // if (foundUser != null) return;
      const updatedUser = await this.userCollection.findOneAndUpdate(
        {
          _id: new ObjectId(__.accountId),
        },
        {
          $push: {
            paymentToken: needToAdd,
          },
        },
        {
          returnDocument: 'after',
        },
      );

      return updatedUser as any;
    } catch (e) {
      console.log(e);
    }
  }

  async deleteCard(id: ObjectId) {
    const user = await this.userCollection.findOne({
      'paymentToken._id': id,
    });
    if (user == null) throw 'User not found';
    const newTokens = (user.paymentToken ?? []).filter((tokens) => {
      return tokens._id.toHexString() != id.toHexString();
    });
    await this.userCollection.updateOne(
      {
        _id: user._id,
      },
      {
        $set: {
          paymentToken: newTokens,
        },
      },
    );
  }
}
