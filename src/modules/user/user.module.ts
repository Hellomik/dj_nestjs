import { Module, forwardRef } from '@nestjs/common';

import { AuthResolver } from './resolvers/auth.resolver';
import { AuthService } from './service/auth.service';
import { AuthorResolver } from './resolvers/author.resolver';
import { CodeService } from './service/code.service';
import { TelegramModule } from '../telegram/telegram.module';
import { WhatsappModule } from '../whatsapp/whatsapp.module';
import { UserResolver } from './resolvers/user.resolver';
import { UserSaveCardService } from './service/user_save_card.service';
import { PaymentModule } from '../payment/payment.module';
import { UserPaymentResolver } from './resolvers/payment.resolver';
import { AuthVetificationController } from './controller/auth_vetification.controller';
import { SendVerificationService } from './service/send_verification.service';

@Module({
  imports: [
    forwardRef(() => TelegramModule),
    forwardRef(() => PaymentModule),
    WhatsappModule,
  ],
  providers: [
    SendVerificationService,
    AuthResolver,
    AuthService,
    AuthorResolver,
    CodeService,
    UserResolver,
    UserSaveCardService,
    UserPaymentResolver,
  ],
  controllers: [AuthVetificationController],
  exports: [CodeService, UserSaveCardService],
})
export class UserModule {}
