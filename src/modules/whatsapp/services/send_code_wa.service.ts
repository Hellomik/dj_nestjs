import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';

@Injectable()
export class SendCodeWaService {
  constructor(private httpService: HttpService) {}

  async sendCode(__: { phoneNumber: string; code: string }) {
    const { phoneNumber: _phoneNumber, code } = __;
    const phoneNumber = cleanPhoneNumber(_phoneNumber);
    try {
      const response = await firstValueFrom(
        this.httpService
          .post('http://104.248.25.76:3100', {
            phone: '+' + phoneNumber,
            code: code,
          })
          .pipe(
            catchError((error: AxiosError) => {
              throw error.cause;
            }),
          ),
      );
    } catch (e) {
      console.log(e);
    }
  }
}
