import { Module } from '@nestjs/common';
import { SendCodeWaService } from './services/send_code_wa.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [SendCodeWaService],
  exports: [SendCodeWaService]
})
export class WhatsappModule {}
