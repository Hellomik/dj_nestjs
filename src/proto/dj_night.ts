/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { Timestamp } from "../../google/protobuf/timestamp.js";

export const protobufPackage = "dj_night";

export enum OrderStatus {
  offer = 0,
  accepted = 1,
  declined = 2,
  started = 3,
  finished = 4,
  UNRECOGNIZED = -1,
}

export interface EmptyBody {
}

export interface ListenPartyInput {
  id?: string | undefined;
}

export interface ListenPartyOutput {
}

export interface Order {
  id?: number | undefined;
  comment?: string | undefined;
  price?: number | undefined;
  priority?: number | undefined;
  acceptTime?: Timestamp | undefined;
  startTime?: Timestamp | undefined;
  finishTime?: Timestamp | undefined;
  status?: OrderStatus | undefined;
  authorId?: string | undefined;
  musicId?: string | undefined;
  userId?:
    | string
    | undefined;
  /** model      Json? */
  partyId?: string | undefined;
}

export const DJ_NIGHT_PACKAGE_NAME = "dj_night";

export interface PartyServiceClient {
  listenPartyAsClient(request: ListenPartyInput): Observable<ListenPartyOutput>;

  listenPartyAsDj(request: EmptyBody): Observable<ListenPartyOutput>;
}

export interface PartyServiceController {
  listenPartyAsClient(request: ListenPartyInput): Observable<ListenPartyOutput>;

  listenPartyAsDj(request: EmptyBody): Observable<ListenPartyOutput>;
}

export function PartyServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["listenPartyAsClient", "listenPartyAsDj"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("PartyService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("PartyService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const PARTY_SERVICE_NAME = "PartyService";
