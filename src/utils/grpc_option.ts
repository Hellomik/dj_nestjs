import { Transport } from '@nestjs/microservices';
import { addReflectionToGrpcConfig } from 'nestjs-grpc-reflection';
import { join } from 'path';

export const grpcClientOptions = addReflectionToGrpcConfig({
  transport: Transport.GRPC,
  options: {
    url: 'localhost:5000',
    package: 'dj_night',
    protoPath: join(__dirname, '../../proto/dj_night.proto'),
  },
});
